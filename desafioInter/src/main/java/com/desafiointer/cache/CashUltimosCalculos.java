package com.desafiointer.cache;

import java.util.LinkedHashMap;
import java.util.Map;

public class CashUltimosCalculos {
Map<String, Integer> chaves = new LinkedHashMap<String, Integer>();
	
	public void recebeUnicoCodigo(int n, int k, int total) {
		String chaveA = Integer.toString(n) + ";" + Integer.toString(k);
		if(chaves.size()==10){
			Object[] chave = chaves.keySet().toArray();
			chaves.remove(chave[0]);
		}
			chaves.put(chaveA, total);
	}
	
	public int unicoDigita(int n, int k) throws NullPointerException{
		Integer as = chaves.get(Integer.toString(n) + ";" + Integer.toString(k));
		return as;
	}
	
	public void listar() {
		System.out.println("\n \t\t\t\t Últimos 10:");
		for (int i : chaves.values()) {
		  System.out.println(i);
		}
	}
}
