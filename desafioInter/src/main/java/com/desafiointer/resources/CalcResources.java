package com.desafiointer.resources;

 	import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.web.bind.annotation.CrossOrigin;
	import org.springframework.web.bind.annotation.GetMapping;
	import org.springframework.web.bind.annotation.PathVariable;
	import org.springframework.web.bind.annotation.PostMapping;
	import org.springframework.web.bind.annotation.RequestBody;
	import org.springframework.web.bind.annotation.RequestMapping;
	import org.springframework.web.bind.annotation.RestController;

	import com.desafiointer.crud.Crud;
	import com.desafiointer.crud.CrudCalculo;
	import com.desafiointer.model.CalculosUsuario;
	import com.desafiointer.model.Users;

import io.swagger.annotations.ApiOperation;

import com.desafiointer.cache.CashUltimosCalculos;

	@RestController
	@RequestMapping(value="/api")
	@CrossOrigin(origins="*")
	public class CalcResources {
		@Autowired
		CrudCalculo crudC;
		CashUltimosCalculos c = new CashUltimosCalculos();
		
		
		@PostMapping("/calcUs")
		@ApiOperation(value="Retorna uma lista de calculos")
		public CalculosUsuario realizaCalc(@RequestBody CalculosUsuario calc) {
			try {
				int total = c.unicoDigita(calc.getValorN(), calc.getValorK());
				calc.setDigUni(total);
			} catch (NullPointerException exeption) {
				calc.setDigUni();
				c.recebeUnicoCodigo(calc.getValorN(), calc.getValorK(), calc.getDigUni());
			}
			c.listar();
			return  crudC.save(calc);	
		}
		
		
		@GetMapping("/calcUs/userId/{id}")
		@ApiOperation(value="Retorna uma lista de calculos por usuarios específico")
		public  List<CalculosUsuario> listaProdutoUnico(@PathVariable(value="id")int id){
			ArrayList<Long> ids = new ArrayList<Long>();
			List<CalculosUsuario> all = crudC.findAll();
			for(int i=0 ; i<all.size() ; i++) {
				if(all.get(i).getIdUser() == id)
					ids.add(all.get(i).getId());
			}
			return crudC.findAllById(ids);
		}
}
