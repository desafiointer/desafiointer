package com.desafiointer.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.desafiointer.crud.Crud;
import com.desafiointer.crud.CrudCalculo;
import com.desafiointer.model.CalculosUsuario;
import com.desafiointer.model.Users;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value="/api")
@CrossOrigin(origins="*")
public class UsersResources {
	@Autowired
	Crud cruds;
	CrudCalculo crudC;
	
	
	@GetMapping("/users")
	@ApiOperation(value="Retorna uma lista usuarios")
	public List<Users> listaUsers(){
		return cruds.findAll();
	}
	
	@PostMapping("/users")
	@ApiOperation(value="Grava uma lista de valores de usuario")
	public Users salvaUser(@RequestBody Users user) {
		return  cruds.save(user);
		
	}
	
	/*@PostMapping("/calcUs")
	
	public CalculosUsuario realizaCalc(@RequestBody CalculosUsuario calc) {
		return  crudC.save(calc);
		
	}*/
	
	/*@PostMapping("/users/{id}")
	public Users salvaUsers(@PathVariable(value="id")long id) {
		return cruds.saveById(id);
	}*/
	
}
