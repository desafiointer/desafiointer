package com.desafiointer.digitounico;

public class DigitoUnico {
		
		//Atributos
		int valorN;
		int valorK;
			
		
		//Construtor
		public DigitoUnico(int unicoN, int unicoK) {
			this.valorN = unicoN;
			this.valorK = unicoK;				
		}
		
		
		
		public DigitoUnico() {
			super();
		}
		
		


		//Métodos
		public int recebeDigN(){
			int num = this.valorN;
			int soma = 0;
			while(num > 0) {
					soma += (num % 10);
					num /= 10;			
			}
				return soma;
		}
		
		public int recebeDigK(){
			int num = valorK;
			int soma = 0;
			
			while(num > 0) {
				 soma += (num % 10);
				 num /= 10;		
			}
			return soma;
		}
		
		public int calculaUnicoDigito() {
			int tot = this.recebeDigK();
			int tot2 = this.recebeDigN();
			int total = tot * tot2;
			int total1 = 0;
			while(total > 0) {
				total1 += (total %10);
				total /= 10;
			}
			System.out.println("O seu digitoUnico é: " + total1);
			return total1;
		}

		
		
	}

