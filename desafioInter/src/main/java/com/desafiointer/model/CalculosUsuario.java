package com.desafiointer.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.desafiointer.digitounico.DigitoUnico;

@Entity
public class CalculosUsuario {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;

	private int valorN;
	private int valorK;
	private int digUni;
	private int idUser;
	
	
	public long getId() {
		return id;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public int getValorN() {
		return valorN;
	}
	public void setValorN(int valorN) {
		this.valorN = valorN;
	}
	public int getValorK() {
		return valorK;
	}
	public void setValorK(int valorK) {
		this.valorK = valorK;
	}
	public int getDigUni() {
		return digUni;
	}

	public void setDigUni() {
		DigitoUnico du = new DigitoUnico(valorN, valorK);
		this.digUni = du.calculaUnicoDigito();
	}
	public void setDigUni(int digUni) {
		this.digUni = digUni;
	}
	
}

