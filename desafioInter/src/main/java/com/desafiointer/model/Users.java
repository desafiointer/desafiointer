package com.desafiointer.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.desafiointer.digitounico.DigitoUnico;

@Entity
public class Users {

	/*public Users(long id, String nome, String email, long valorN, long valorK, int digUni) {
		super();
		this.id = id;
		this.nome = nome;
		this.email = email;
		this.valorN = valorN;
		this.valorK = valorK;
		this.digUni = digUni;
	}
	public Users(long id, String nome, String email, long valorN, long valorK) {
		super();
		this.id = id;
		this.nome = nome;
		this.email = email;
		this.valorN = valorN;
		this.valorK = valorK;
	}*/
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;

	private String nome;
	private String email;
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
}
