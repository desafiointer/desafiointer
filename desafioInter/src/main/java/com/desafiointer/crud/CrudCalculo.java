package com.desafiointer.crud;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.desafiointer.cache.CashUltimosCalculos;
import com.desafiointer.digitounico.DigitoUnico;
import com.desafiointer.model.CalculosUsuario;
import com.desafiointer.model.Users;
import com.desafiointer.resources.UsersResources;


public interface CrudCalculo extends JpaRepository<CalculosUsuario, Long>{

	CalculosUsuario findById(long id);
	
}
