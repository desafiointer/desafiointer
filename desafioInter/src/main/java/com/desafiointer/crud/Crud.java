package com.desafiointer.crud;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.desafiointer.digitounico.DigitoUnico;
import com.desafiointer.model.CalculosUsuario;
import com.desafiointer.model.Users;
import com.desafiointer.resources.UsersResources;


public interface Crud extends JpaRepository<Users, Long>{
	
}
