package com.desafiointer;

import java.util.Scanner;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.desafiointer.cache.CashUltimosCalculos;
import com.desafiointer.digitounico.DigitoUnico;

@SpringBootTest
class DesafiointerApplicationTests {

	@Test
	void contextLoads() {
		System.out.print("Digite o valor de N: ");
		Scanner scan = new Scanner(System.in);
		int digi1 = scan.nextInt();
		
		System.out.print("Digite o valor de K: ");
		int digi2 = scan.nextInt();
		DigitoUnico dig = new DigitoUnico(digi1, digi2);		
		int total = dig.calculaUnicoDigito();
		CashUltimosCalculos c = new CashUltimosCalculos();
		c.recebeUnicoCodigo(digi1, digi2, total);
		try {
			total = c.unicoDigita(digi1, digi2);
		} catch (NullPointerException exeption) {
			total = dig.calculaUnicoDigito();
			c.recebeUnicoCodigo(digi1, digi2, total);
		}
		
		
		c.listar();
	}

	}


